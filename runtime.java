import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mx.bigdata.jcalais.CalaisObject;

public class runtime {

	public static void main(String[] args) throws IOException {

		/*
		 * -------------------------------- RUN TIME
		 * --------------------------------
		 */
		
		// this runs thru baseline.res and returns an Arraylist of files per
		// topic
		// Topic file map is a hash map of topics that has list of entries
		Map<Integer, Topic> TopicMap = fetchTopics.getTopEntriesObject();

		// initiate the calais api
		ApiUsage calaisApiInstance = new ApiUsage();
		calaisApiInstance.setupClient();

		// Items for loop
	
		// --------------------------------
		// Each Topic
		for (int TopicNumber = 1; TopicNumber <= 25; TopicNumber++ ) {

			// --------------------------------
			// Each Article
			int articleCounter = 1;
			for (Article article : TopicMap.get(TopicNumber).ListOfArticles) {
				// analyzeterm calls getContent on article which will ensure
				// article-object has been parsed
				HashMap<String, Double> entities = calaisApiInstance
						.analyzeTermsWithCache(article);
				// --------------------------------
				// Each Entity
				for (String term : entities.keySet()) {
					article.putTermScore(term, entities.get(term));
				}
				 System.out.println("Finished reading file("+articleCounter+") '"+article.getPath()+"' ("+article.getDocno()+")"); //logging per file read
				// to only run thru 5 articles for testing
				// if ( articleCounter == 5 ){
				// break;
				// }
				articleCounter++;

			}
			

		}
		
		
		int count;
		/* ---------------------------------
		 	Top 5
		--------------------------------- */
		PrintWriter writer = new PrintWriter( "results/groupID_resultTop5.txt", "UTF-8");
		for ( int TopicNumber = 1; TopicNumber <= 25; TopicNumber++ ) {
			TopicMap.get(TopicNumber).getCosines(5);
			//for retrieving the article post sort
			HashMap<String,Article> articleMap = new HashMap<String,Article>(); 
			HashMap<String,Double> docCosineScore = new HashMap<String,Double>();
			
			for ( Article article : TopicMap.get(TopicNumber).ListOfArticles ){
				articleMap.put( article.getDocno(), article );
				docCosineScore.put( article.getDocno(), article.getCosine() );
			}
			
			HashMap<String,Double> sorted = Utilities.sortHashMapByValuesD(docCosineScore);
			count = 0;
			for ( String docno : sorted.keySet() ){
				Article article = articleMap.get(docno);
				writer.println( 
					TopicNumber + " "
					+ "Q0" + " "
					+ article.getDocno() + " "
					+ count + " "
					+ article.getCosine() + " "
					+ "Cosine"
				);
				count++;
			}
		}
		writer.close();
		/* ---------------------------------
	 		end of top 5
		--------------------------------- */
		
		/* ---------------------------------
		 	Top 10
		--------------------------------- */
		writer = new PrintWriter( "results/groupID_resultTop10.txt", "UTF-8");
		for ( int TopicNumber = 1; TopicNumber <= 25; TopicNumber++ ) {
			TopicMap.get(TopicNumber).getCosines(10);
			//for retrieving the article post sort
			HashMap<String,Article> articleMap = new HashMap<String,Article>(); 
			HashMap<String,Double> docCosineScore = new HashMap<String,Double>();
			
			for ( Article article : TopicMap.get(TopicNumber).ListOfArticles ){
				articleMap.put( article.getDocno(), article );
				docCosineScore.put( article.getDocno(), article.getCosine() );
			}
			
			HashMap<String,Double> sorted = Utilities.sortHashMapByValuesD(docCosineScore);
			count = 0;
			for ( String docno : sorted.keySet() ){
				Article article = articleMap.get(docno);
				writer.println( 
					TopicNumber + " "
					+ "Q0" + " "
					+ article.getDocno() + " "
					+ count + " "
					+ article.getCosine() + " "
					+ "Cosine"
				);
				count++;
			}
		}
		writer.close();
		/* ---------------------------------
	 		end of top 10
		--------------------------------- */	
	
	
		/* ---------------------------------
		 	Top 20
		--------------------------------- */
		writer = new PrintWriter( "results/groupID_resultTop20.txt", "UTF-8");
		for ( int TopicNumber = 1; TopicNumber <= 25; TopicNumber++ ) {
			TopicMap.get(TopicNumber).getCosines(20);
			//for retrieving the article post sort
			HashMap<String,Article> articleMap = new HashMap<String,Article>(); 
			HashMap<String,Double> docCosineScore = new HashMap<String,Double>();
			
			for ( Article article : TopicMap.get(TopicNumber).ListOfArticles ){
				articleMap.put( article.getDocno(), article );
				docCosineScore.put( article.getDocno(), article.getCosine() );
			}
			
			HashMap<String,Double> sorted = Utilities.sortHashMapByValuesD(docCosineScore);
			count = 0;
			for ( String docno : sorted.keySet() ){
				Article article = articleMap.get(docno);				
				writer.println( 
					TopicNumber + " "
					+ "Q0" + " "
					+ article.getDocno() + " "
					+ count + " "
					+ article.getCosine() + " "
					+ "Cosine"
				);
				count++;
			}
		}
		writer.close();
		/* ---------------------------------
				end of top 20
		--------------------------------- */
		
		
	}


}

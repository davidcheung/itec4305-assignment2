import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import mx.bigdata.jcalais.CalaisClient;
import mx.bigdata.jcalais.CalaisConfig;
import mx.bigdata.jcalais.CalaisObject;
import mx.bigdata.jcalais.CalaisResponse;
import mx.bigdata.jcalais.rest.CalaisRestClient;

public class ApiUsage {

	public CalaisClient client;
	CalaisConfig config;
	public Iterable<String> fieldNames;
	protected String cache_folder = "./api-result-cache/"; 

	public void setupClient() {
		client = new CalaisRestClient("w56bvu9dk55ebpkxrb9b34c5");
		config = new CalaisConfig();
		config.set(CalaisConfig.ProcessingParam.CALCULATE_RELEVANCE_SCORE,"true");		
		config.set(CalaisConfig.ConnParam.CONNECT_TIMEOUT, 0);
	    config.set(CalaisConfig.ConnParam.READ_TIMEOUT, 0);
	}
	
	public ArrayList<String> analyze(String article){
		ArrayList<String> terms = new ArrayList<String>();
		try {
	    	 CalaisResponse response = client.analyze(article,config);
	    	 for(CalaisObject entity : response.getEntities()){
					terms.add(entity.getField("name"));	
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return terms; 
	}
	
	
	//this might be the only function we need to keep, returns the terms
	public ArrayList<CalaisObject> analyzeTerms(Article article){
		ArrayList<CalaisObject> terms = new ArrayList<CalaisObject>();
		
		try {
	    	 CalaisResponse response = client.analyze(article.getContent(),config);
	    	 for(CalaisObject entity : response.getEntities()){
	    		 terms.add(entity);	
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return terms; 
	}
	
	
	protected HashMap<String,Double> readCacheFile( String path ){
		HashMap<String,Double> output = new HashMap<String,Double>();
		String content;
		try {
			content = Utilities.readFile(path);
			if ( content.isEmpty() ){
				return output;
			}
			String[] lines = content.split("\n");	
			if ( lines.length > 0 ){
				for ( String line : lines ){
					String[] items = line.split(",");
					if ( items.length > 2 ){
						//means the term itself has commas
						String term = "";
						for ( int i = 0; i<items.length-1;i++){ 
							term = term + items[i];
						}
						output.put( items[0], Double.parseDouble(items[(items.length-1)]));
					}
					else{
						output.put( items[0], Double.parseDouble(items[1]));
					}
					
				}	
			}
			return output;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return output;
		}
		
		
		
		
	}
	
	public HashMap<String,Double> analyzeTermsWithCache(Article article) throws FileNotFoundException, UnsupportedEncodingException{
		
		//caching the api results for faster testing
		String cachedPath =  this.cache_folder + article.getDocno()+".cache";		
		HashMap<String,Double> readFromCache = readCacheFile(cachedPath);		 
		if ( !readFromCache.isEmpty() ) {
			return readFromCache;
		}
			
		HashMap<String,Double> terms = new HashMap<String,Double>();
		
		try {
			 PrintWriter writer = new PrintWriter( cachedPath , "UTF-8");
	    	 CalaisResponse response = client.analyze(article.getContent(),config);
	    	 for(CalaisObject entity : response.getEntities()){
	    		 writer.println( entity.getField("name") + ","+ entity.getField("relevance") );
	    		 terms.put(entity.getField("name"), Double.parseDouble(entity.getField("relevance")) );	
			}
	    	writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return terms; 
	}
	
	public void analyzePrint(String article){
		try {
			CalaisResponse response = client.analyze(article,config);
			System.out.println( response.getMeta() + "\n ---------------------------------");
			
			for (CalaisObject entity : response.getEntities()) {
				fieldNames = entity.getFieldNames();
		        System.out.println(entity.getField("_type") + ":" 
                   + entity.getField("name") + " \n"+ entity.toString() + "\n\n" );
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class Article {

	String path;
	boolean parsed = false;
	Document doc;
	Map<String,Double> TermsScoreMap = new HashMap<String,Double>();
	String docno;
	double score;
	double cosine;
	int index;
	
	public Article(String filepath){
		this.setPath( filepath );
	}
	
	public String setPath( String path ){
		this.path = path;
		return this.path;
	}
	public String getPath(){
		return this.path;
	}
	public String setDocno(String docno){
		this.docno = docno;
		return docno;
	}
	public String getDocno(){
		return this.docno;
	}
	
	public Document parsArticle() throws ParserConfigurationException, SAXException, IOException{
		File fXmlFile = new File(this.getPath());
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		this.doc = doc;
		return doc;
	}
	
	public String getContent(){
		if ( !this.parsed ){
			try {
				this.parsArticle();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String result = this.doc.getElementsByTagName("TEXT").item(0).getTextContent();
		return result;
	}
	
	public void putTermScore(String term, Double score){
		this.TermsScoreMap.put(term, score);
	}
	
	public double setArticleScore( double score ){
		this.score = score;
		return this.score;
	}
	
	public double getArticleScore(){
		return this.score;
	}
	
	public double setCosine( double cosine ){
		this.cosine= cosine;
		return this.cosine;
	}
	
	public double getCosine(){
		return this.cosine;
	}
	
	public int setIndex( int index ){
		this.index = index;
		return index;
	}
	public int getIndex(){
		return this.index;
	}
}

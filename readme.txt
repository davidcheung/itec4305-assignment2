------------------------------
    File/folder Structure
------------------------------
[Project Folder]
├── lib/
│    ├── guava-16.0.1.jar
│    ├── jackson-core-asl-1.9.13.jar
│    ├── jackson-mapper-asl-1.9.13.jar
│    ├── j-calais-1.0.jar
│
├── dataset/
│    ├── 13355813.xml
│    ├── 13355843.xml
│    ├── ..... all the xml files
│
├── resources/
│    ├── baseline.res
│
├── results/
├── api-result-cache/
│
├── runtime.java
├── fetchTopics.java
├── Topic.java
├── Utilities.java
├── Article.java
├── runtime.java


------------------------------
    Installation / Run
------------------------------
 - Open Eclipse
 - Create a new Java Project with [folder root as source code and class]
 - navigate the the project folder and paste source code in as folder structure
 - make sure "api-results-cache" and "results" folder exist and are `chmod` writable
 - paste the raw xml dataset in "dataset/" folder
 - associate the packages in "lib/" to the project
 - run the runtime.java as an application
 - the results should be created and populated as txt files in
     [Project Folder]
     ├── results/
     │      ├── top5results.txt
     │      ├── top10results.txt
     │      ├── top20results.txt

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;


public class Topic {

	protected int TopicNumber;
	public ArrayList<Article> ListOfArticles;
	public int termCount = 0;
	
	public Topic( int topicNumber ){
		//construct
		this.ListOfArticles = new ArrayList<Article>();
		this.setTopicNumber(topicNumber);
	}
	
	public int setTopicNumber( int topicNumber ){
		this.TopicNumber = topicNumber;
		return this.TopicNumber;
	}
	
	public int getTopicNumber(){
		return this.TopicNumber;
	}

	
	public HashMap<String, Double> getTopTF( int amount_of_articles_to_care_about ){
		int counter = 0;
		ArrayList<String> terms = new ArrayList<String>();
		HashMap<String, Double> TermFrequency = new HashMap<String, Double>();
		//looping thru the first topic 
		for( Article article : this.ListOfArticles ){
			if(counter < amount_of_articles_to_care_about ){
				//
				for(String eachTerm: article.TermsScoreMap.keySet() ){
					//casting the term string to lowercase
					String lowercasedterm = eachTerm.toLowerCase();
					//if doesnt exist set it as 1
					if(!terms.contains( lowercasedterm) ){
						terms.add( lowercasedterm );
						TermFrequency.put( lowercasedterm,1.0);						
					}
					//if exist then add 1
					else if(terms.contains(lowercasedterm)){
						Double newFrequency = TermFrequency.get(lowercasedterm)+1.0;
						TermFrequency.put(lowercasedterm, newFrequency );
					}
					termCount++;
				}
				counter++;
			}
		}
		return TermFrequency;
	}
	
	public void TopicSummarizeInAFile() throws FileNotFoundException, UnsupportedEncodingException{
		String folder = "./results/";
		String filename = "Topic "+this.TopicNumber + ".txt";
		PrintWriter writer = new PrintWriter( folder + filename, "UTF-8");
		writer.println( "===========================================" );
		writer.println( "Topic " + this.TopicNumber );
		writer.println( "===========================================" );
		for ( Article article : this.ListOfArticles ){
			writer.println(  "-----------------------------------" );
			writer.println( "DOCNO " + article.getDocno() );
			writer.println(  "-----------------------------------" );
			for ( String term :  article.TermsScoreMap.keySet() ){
				writer.println( term + " - " + article.TermsScoreMap.get(term));
			}
		}
		writer.close();
		System.out.println( "###### Written to " + folder + filename );
		
	}
	
	public HashMap<String, Double> getTopIDF(int NumberOfArticles ){
		HashMap<String, Double> TermFrequencyMap = this.getTopTF(NumberOfArticles);
		HashMap<String, Double> IDFMap = new HashMap<String, Double>();
		//loop through all the terms 
		for ( String term : TermFrequencyMap.keySet() ){
			double frequency = TermFrequencyMap.get(term);
			double idf = (Double) Math.log( NumberOfArticles / frequency );
			IDFMap.put( term , idf );
		}
		return IDFMap;
	}
	
	
	public void getCosines( int NumberOfArticles ){
		
		//result 
		
		//getting needed data for calculation
		HashMap<String,Double> IDF = this.getTopIDF(NumberOfArticles);
		
		for ( Article article : this.ListOfArticles ){
			//setting up variables
			double product = 0; 
			double query = 0;
			double documentScore = 0;
			double cosine;
			
			for( String term : article.TermsScoreMap.keySet() ){
				String termlowercase = term.toLowerCase();
				if ( IDF.containsKey(termlowercase)){
					double termIDF = IDF.get(termlowercase);
					double termFrequery = article.TermsScoreMap.size();
					product = product + termIDF * ( 1.0 / termFrequery);
					query = query + Math.sqrt( (1/termFrequery) * (termIDF) );
					documentScore = documentScore + Math.sqrt( Math.pow(termIDF,2)  );
				}
			}
			cosine = product / ( query * documentScore );
			if ( Double.isNaN(cosine) ){
				article.setCosine(0);
			}else{
				article.setCosine(cosine);
			}
			
		}
		
		
		
	}
	
	
	
	
	
}

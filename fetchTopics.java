import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class fetchTopics {
	
	static String path_prefix = "/dataset";
	static String entryfile ="resources/baseline.res";
	
	public static Map<Integer,Topic> getTopEntriesObject() throws IOException{
		//initializing variable for the map of file names that gets returned
		
		Map<Integer,Topic> ListOfTopics = new HashMap<Integer,Topic>();
		//reading the file
		String content = Utilities.readFile(fetchTopics.entryfile);
		//spliting the lines
		String[] lines = content.split("\n");
		//creating a empty arraylist for every topic
		
		for ( int i = 1; i <= 25; i++ ){
			Topic topic = new Topic(i);
			ListOfTopics.put( i , topic );
		}
		//putting the files into each arraylist (per topic)
		for ( int i=0; i< lines.length ;i++  ){
			//splitting each line by space
			String[] temp = lines[i].split("\\s+");
			int topic = Integer.parseInt(temp[0]);
			//generating file name by regular expression parsing out all non-numbers
			String filename = temp[2].replaceAll("[A-Za-z-]","")+".xml"; 
			//limiting each topic to 50 files
			if ( ListOfTopics.get(topic).ListOfArticles.size() < 50 ){
				Article article = new Article( "."+fetchTopics.path_prefix +"/" + filename);
				article.setDocno(temp[2]);
				article.setIndex(Integer.parseInt(temp[3]));
				ListOfTopics.get(topic).ListOfArticles.add(article);
			}
		}
		
		return ListOfTopics;
	}
	
	
	
}
